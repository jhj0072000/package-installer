// example 1
const input1 = ["KittenService: CamelCaser", "CamelCaser: "];

// example 2
const input2 = [
    "KittenService: ",
    "Leetmeme: Cyberportal",
    "Cyberportal: Ice",
    "CamelCaser: KittenService",
    "Fraudstream: Leetmeme",
    "Ice: "
];

// example 3
const input3 = [
    "KittenService: ",
    "Leetmeme: Cyberportal",
    "Cyberportal: Ice",
    "CamelCaser: KittenService",
    "Fraudstream: ",
    "Ice: Leetmeme"
];

const packageInstaller = function(pkgArr) {
    let results = [];
    let graph = {};
    let sorted = {};

    // check data type
    if (pkgArr === null) throw 'input required';
    if (!Array.isArray(pkgArr)) throw 'data must be an array';
    pkgArr.forEach((pair) => {
        if (typeof pair !== 'string') throw 'each item should be string';
    });

    // create pkg:dep graph
    pkgArr.forEach(set => {
        const packageAndDependency = set.split(':');
        // check for missing colons
        if (packageAndDependency.length !== 2) throw 'colon missing';
        const pkg = packageAndDependency[0].trim();
        const dep = packageAndDependency[1].trim();

        // check for missing package name
        if (pkg.length === 0) throw 'package missing';

        if (!graph[pkg]) graph[pkg] = [];
        if (!graph[dep] && dep.length > 0) graph[dep] = [];
        if (dep.length > 0) graph[pkg].push(dep);
    });

    // search through visit
    Object.keys(graph).forEach(pkg => {
        visit(pkg, []);
    });

    // visit
    function visit(pkg, visited) {
        if (sorted[pkg]) return;
        visited.push(pkg);
        const dependencies = graph[pkg];
        dependencies.forEach(dependency => {
            if (visited.indexOf(dependency) >= 0) throw 'circular dependency found';
            visit(dependency, visited);
        });
        sorted[pkg] = true;
        results.push(pkg);
    }
    return results.join(', ');
}

module.exports = packageInstaller;