const packageInstaller = require('./packageInstaller');

test ('Sample #1', () => {
    const test = ["KittenService: CamelCaser", "CamelCaser: "];
    expect(packageInstaller(test)).toBe('CamelCaser, KittenService');
});

test ('Sample #2', () => {
    const test = ["KittenService:","Leetmeme: Cyberportal","Cyberportal: Ice","CamelCaser: KittenService","Fraudstream: Leetmeme","Ice:"];
    expect(packageInstaller(test)).toBe('KittenService, Ice, Cyberportal, Leetmeme, CamelCaser, Fraudstream');
});

test ('Sample #3', () => {
    const test = ["KittenService: ","Leetmeme: Cyberportal","Cyberportal: Ice","CamelCaser: KittenService","Fraudstream: ","Ice: Leetmeme"];
    expect(() => {
        packageInstaller(test)
    }).toThrow();
});

test ('Should Fail When Colon Missing', () => {
    const test = ["hello", "hello:bye"];
    expect(() => {
        packageInstaller(test)
    }).toThrow();
});

test ('Should Fail Inputs Other Than Array', () => {
    const types = ['string', { object: 'object' }, 3, true];
    types.forEach(type => {
        expect(() => {
            packageInstaller(type)
        }).toThrow();
    });
});

test ('Should Fail Array Item Other Than String', () => {
    const types = [{ object: 'object' }, 3, true];
    types.forEach(type => {
        expect(() => {
            packageInstaller(type);
        }).toThrow();
    })
});

test ('Can Handle Spaces', () => {
    const test = [" hello :    ", "hello : bye   "];
    expect(packageInstaller(test)).toBe('bye, hello');
});

