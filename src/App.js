import React, { Component } from 'react';
import _ from 'lodash';
import styled from 'styled-components';

const MainWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100vh;
`;

const AppWrapper = styled.div`
  border: 1px solid black;
  padding: 50px;
`;

const Title = styled.div`
  font-size: 60px;
  margin-bottom: 25px;
  text-align: center;
`;

const PackageInput = styled.input`
  height:  50px;
  width: 600px;
  border: 1p solid black;
  font-size: 35px;
  padding: 0 10px 0 10px;
`;

const OutputArea = styled.div`
  background: ${props => {
    if (props.error) return '#fab1a0';
    return '#55efc4';
  }};
  padding: 10px;
  margin-top: 25px;
  width: 604px;
  overflow-x: scroll;
  overflow-y: hidden;
  white-space: nowrap;
  &::-webkit-scrollbar {
    background: #dfe6e9;
    height: 6px;
  }
  &::-webkit-scrollbar-thumb {
    background: gray; 
    border-radius: 10px;
  }
`;

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      answer: _.noop()
    };
  }

  updateInput = (e) => {
    this.setState({ input: e.target.value });
  }

  objectifyPackages = (arr) => {
    let error;
    let packageObject = {};

    // Check for colons
    _.forEach(arr, item => {
      const packageAndDependency = _.split(item, ':');
      if (packageAndDependency.length !== 2) {
        error = 'colon';
        return false;
      }
      const packageName = _.trim(_.get(packageAndDependency, '[0]'));
      const dependencyName = _.trim(_.get(packageAndDependency, '[1]'));

      if (packageName.length === 0) {
        error = 'package-empty';
        return false;
      }

      // TODO: create package:dependency object
      if (!packageObject[packageName]) packageObject[packageName] = [];
      if (!packageObject[dependencyName]) packageObject[dependencyName] = [];
      if (dependencyName.length > 0) packageObject[packageName].push(dependencyName);
    });
    if (error) return error;
    return packageObject;
  }

  topologicalSort = (packageObject) => {
    let error;
    let results = [];
    let sorted = {};

    _.forEach(_.keys(packageObject), pkg => {
      sort(pkg, []);
    })

    function sort(pkg, ancestors) {
      if (sorted[pkg]) return;
      ancestors.push(pkg);
      const dependencies = _.get(packageObject, `[${pkg}]`, _.noop());
      _.forEach(dependencies, dependency => {
        if (_.indexOf(ancestors, dependency) >= 0) {
          error = 'circular';
          return false;
        }
        sort(dependency, ancestors);
      });
      _.set(sorted, `[${pkg}]`, true);
      results.push(pkg);
    }
  
    if (error) return error;
    return results;
  }

  handleOutput = (e, input) => {
    if (e) {
      e.preventDefault();
      e.stopPropagation();
    }
    if (input) {
      try {
        // Replacing any single quotes into double quotes before parsing string into array
        const array = JSON.parse(input.replace(/'/g, '"'));

        // Checks if all items are strings
        _.forEach(array, item => {
          if (typeof item !== 'string') throw { name: 'typeof', message: 'All items in the array must be strings' };
        });

        // Objectify the array
        const packageObject = this.objectifyPackages(array);
        if (packageObject === 'colon') throw { name: 'colon', message: 'Each item should follow this format: "packageName: dependencyName".' };
        if (packageObject === 'package-empty') throw { name: 'package-empty', message: 'Package name cannot be empty.' };

        // TopSort
        const sorted = this.topologicalSort(packageObject);
        if (sorted === 'circular') throw { name: 'circular', message: 'Circular dependency detected.' };

        this.setState({
          error: _.noop(),
          answer: _.join(_.filter(sorted, p => p !== ''), ', ')
        });
        return _.join(_.filter(sorted, p => p !== ''), ', ');
      } catch (e) {
          switch(e.name) {
            case 'typeof':
              this.setState({ error: e.message });
              break;
            case 'colon':
              this.setState({ error: e.message });
              break;
            case 'package-empty':
              this.setState({ error: e.message });
              break;
            case 'circular':
              this.setState({ error: e.message });
              break;
            default:
              this.setState({ error: 'Invalid array format!' });
              break;
          }
          this.setState({ answer: _.noop() })
          return e.message;
      }
    }
  }

  render() {
    const { answer, error } = this.state;
    // console.log(this.state);
    return (
      <MainWrapper>
        <AppWrapper>
          <Title>Package Installer</Title>
          <form onSubmit={(e) => this.handleOutput(e, this.state.input)}>
            <PackageInput
              placeholder="Array of packages and dependencies"
              answer={answer}
              onChange={this.updateInput} />
          </form>
          {error &&
            <OutputArea error={error}>{error || answer}</OutputArea>}
          {answer && 
            <OutputArea>{answer}</OutputArea>}
        </AppWrapper>
      </MainWrapper>
    );
  }
}

export default App;
