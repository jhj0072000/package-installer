import React from 'react';
import App from './App';
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({adapter: new Adapter()});

it('Sample #1', () => {
  const wrapper = shallow(<App />);
  const test = wrapper.instance().handleOutput(undefined, '["KittenService: CamelCaser", "CamelCaser:"]');
  expect(test).toEqual('CamelCaser, KittenService');
});

it('Sample #2', () => {
  const wrapper = shallow(<App />);
  const test = wrapper.instance().handleOutput(undefined, '["KittenService:","Leetmeme: Cyberportal","Cyberportal: Ice","CamelCaser: KittenService","Fraudstream: Leetmeme","Ice:"]');
  expect(test).toEqual('KittenService, Ice, Cyberportal, Leetmeme, CamelCaser, Fraudstream');
});

it('Sample #3 with circular dependency', () => {
  const wrapper = shallow(<App />);
  const test = wrapper.instance().handleOutput(undefined, '["KittenService: ","Leetmeme: Cyberportal","Cyberportal: Ice","CamelCaser: KittenService","Fraudstream: ","Ice: Leetmeme"]');
  expect(test).toEqual('Circular dependency detected.');
});

it('Colon missing', () => {
  const wrapper = shallow(<App />);
  const test = wrapper.instance().handleOutput(undefined, '["package/dependency", "package,depdency"]');
  expect(test).toEqual('Each item should follow this format: \"packageName: dependencyName\".');
})

// TODO: should return error if array item is anything but string
// TODO: shuold return error if pkg name is empty
// TODO: should return error if circular dependency detected
// TODO: should return error if input is invalid array format
// TODO: can handle single/double quotes
// TODO: can handle special characters
// TODO: can handle items out of order
// TODO: can handle empty spaces